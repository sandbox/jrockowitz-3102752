<?php

namespace Drupal\commerce_webform_order_demo\Twig;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Twig extension for Commerce Webform Order demo.
 */
class CommerceWebformOrderDemoTwigExtension extends \Twig_Extension {

  use StringTranslationTrait;

  /**
   * Element keys using for transactions.
   *
   * @var array
   */
  protected $transactionElementKeys;

  /**
   * Cache transactions.
   *
   * @var array
   */
  protected $transactions = [];

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('commerce_webform_order_demo_transaction', [$this, 'transaction']),
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('dollars', [$this, 'dollars']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'commerce_webform_order_demo';
  }

  /**
   * Format a value in USD dollars.
   *
   * @param int $value
   *   A value.
   *
   * @return string
   *   The value converted to dollars
   */
  public function dollars($value) {
    return '$' . number_format($value,2,'.',',');
  }

  /**
   * Create a commerce transaction.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return array
   *   A commerce transaction.
   */
  public function transaction(array $variables) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $variables['webform_submission'];
    $webform = $webform_submission->getWebform();
    $transaction_function = $webform->id();
    return $this->$transaction_function($variables);

  }

  protected function getWebform(array $variables) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $variables['webform_submission'];
    return $webform_submission->getWebform();
  }

  /**
   * Get products.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return array
   *   Products.
   *
   * @see /admin/structure/webform/manage/commerce_webform_order_demo/settings/form
   */
  protected function getProducts(array $variables) {
    $elements = $this->getWebform($variables)->getElementsDecoded();
    return $elements['#products'];
  }

  /**
   * Get coupons.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return array
   *   Coupons.
   *
   * @see /admin/structure/webform/manage/commerce_webform_order_demo/settings/form
   */
  protected function getCoupons(array $variables) {
    $elements = $this->getWebform($variables)->getElementsDecoded();
    return $elements['#coupons'];
  }

  /**
   * Get all transaction element keys.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return array
   *   All transaction element keys.
   */
  protected function getTransactionKeys(array $variables) {
    $products = $this->getProducts($variables);
    $element_keys = array_keys($products) + ['coupon' => 'coupon'];
    return array_combine($element_keys, $element_keys);
  }

  /**
   * Get the transaction id.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return string
   *   The transaction id.
   */
  protected function getTransactionId(array $variables) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $variables['webform_submission'];
    $data = $webform_submission->getData();
    $transaction_data = array_intersect_key($data, $this->getTransactionKeys($variables));
    return md5(serialize($transaction_data));
  }

  /**
   * Perform transaction for Commerce Webform Order Demo.
   *
   * @param array $variables
   *   Every computed Twig template variable.
   *
   * @return array
   *   A commerce transaction.
   */
  protected function commerce_webform_order_demo(array $variables) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $variables['webform_submission'];

    $transaction_id = $this->getTransactionId($variables);
    if (isset($this->transactions[$transaction_id])) {
      return $this->transactions[$transaction_id];
    }

    // Get products.
    $products = $this->getProducts($variables);
    // @todo Apply product coupons.
    // @todo Adjust costs based on submission data.

    // Get coupon.
    $coupons = $this->getCoupons($variables);
    $coupon_code = $webform_submission->getElementData('coupon');
    $coupon = (isset($coupons[$coupon_code])) ? $coupons[$coupon_code] : [];

    // Get amounts.
    $amount = [
      'total' => ['base' => 0, 'discount' => 0, 'final' => 0],
      'paid' => ['base' => 0, 'discount' => 0, 'final' => 0],
      'due' => ['base' => 0, 'discount' => 0, 'final' => 0],
    ];

    // Get purchased and order items.
    $purchased = [];
    $order = [];

    foreach ($products as $product_name => $product) {
      $product_value = $webform_submission->getElementData($product_name);
      if ($product_value) {
        $product_purchased = $webform_submission->getElementData($product_name .'_purchased');

        $product_base = $product['cost'];
        $product_discount = ($coupon) ? ($coupon['rate'] * $product_base) : 0;
        $product_final = $product_base - $product_discount;

        $product['base'] = $product_base;
        $product['discount'] = $product_discount;
        $product['final'] = $product_final;

        $amount['total']['base'] += $product_base;
        $amount['total']['discount'] += $product_discount;
        $amount['total']['final'] += $product_final;

        if ($product_purchased) {
          $amount['paid']['base'] += $product_base;
          $amount['paid']['discount'] += $product_discount;
          $amount['paid']['final'] += $product_final;
          $purchased[$product_name] = $product;
        }
        else {
          $amount['due']['base'] += $product_base;
          $amount['due']['discount'] += $product_discount;
          $amount['due']['final'] += $product_final;
          $order[$product_name] = $product;
        }
      }
    }

    // Build the transaction.
    $transaction = [];
    $transaction['products'] = $products;
    $transaction['purchased'] = $purchased;
    $transaction['order'] = $order;
    $transaction['coupon'] = $coupon;
    $transaction['amount'] = $amount;

    // Cache the transaction to improve performance.
    $this->transactions[$transaction_id] = $transaction;

    return $transaction;
  }

}
