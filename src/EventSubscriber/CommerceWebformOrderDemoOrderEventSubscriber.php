<?php

namespace Drupal\commerce_webform_order_demo\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\Entity\WebformSubmission;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class CommerceWebformOrderDemoOrderEventSubscriber.
 *
 * @package Drupal\commerce_webform_order_demo
 */
class CommerceWebformOrderDemoOrderEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];
    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param WorkflowTransitionEvent $event
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $items = $order->getItems();
    foreach ($items as $item) {
      // Check for related webform submission.
      if (empty($item->commerce_webform_order_submissions)
        || empty($item->commerce_webform_order_submissions->target_id)) {
        continue;
      }

      $sid = $item->commerce_webform_order_submissions->target_id;

      /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
      $webform_submission = WebformSubmission::load($sid);
      $webform = $webform_submission->getWebform();
      $elements = $webform->getElementsInitializedAndFlattened();

      // Mark items as purchased.
      $data = $webform_submission->getData();
      foreach ($data as $element_key => $value) {
        if (!$value || !isset($elements[$element_key . '_purchased'])) {
          continue;
        }
        $data[$element_key . '_purchased'] = 1;
      }

      // Make the coupon as applied.
      $data['coupon_applied'] = 1;
      $webform_submission->setData($data)->resave();

      $t_args = [
        '@title' => $webform->label(),
        ':href' => $webform->toUrl()->toString(),
      ];
      \Drupal::messenger()->addStatus($this->t('Add more products to your <a href=":href">@title</a> order.', $t_args));
    }
  }

}
